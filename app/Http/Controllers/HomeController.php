<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Game;
use App\Models\Genre;
use App\Models\Message;
use App\Models\Page;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    // Section Home
    public function home_index()
    {
        // Menampilkan data game yang berstatus publish
        $games = Game::query()->where('status', 'publish');
        $games = $games->latest()->paginate(4);
        // Menampilkan data post yang berstatus publish
        $posts = Post::query()->where('status', 'publish');
        $posts = $posts->latest()->paginate(4);
        return view('home.index', compact('games', 'posts'));
    }
    // End Section Home

    // Section Game
    public function game_index()
    {
        // Menampilkan data game yang berstatus publish
        $games = Game::query()->where('status', 'publish');
        // Mencari post berdasarkan nilai search
        if (request('search')) {
            $games->where('title', 'LIKE', '%' . request('search') . '%');
        }
        $search = request('search') ?? '';

        $games = $games->latest()->paginate(4);

        // Data Genre
        $genres = Genre::all();

        return view('home.game.index', compact('games', 'genres', 'search'));
    }

    public function game_detail($slug)
    {
        $game = Game::query()->where('slug', $slug)->first();
        return view('home.game.detail', compact('game'));
    }

    public function game_genre($slug)
    {
        $genres = Genre::where('slug', $slug)->first();
        $games = $genres->games()->where('status', 'publish')->latest()->paginate(4);
        $search = request('search') ?? '';

        $found = Genre::find($genres->id);
        $genres = Genre::all();
        $devices = Tag::all();
        return view('home.game.genre', compact('games', 'genres', 'devices', 'search', 'found'));
    }
    // End Section Game

    // Section Blog
    public function blog_index()
    {
        // Menampilkan data post yang berstatus publish
        $posts = Post::query()->where('status', 'publish');
        // Mencari post berdasarkan nilai search
        if (request('search')) {
            $posts->where('title', 'LIKE', '%' . request('search') . '%');
        }
        $search = request('search') ?? '';
        $posts = $posts->latest()->paginate(4);

        // Menampilkan data post berdasarkan category
        $categories = Category::all();
        $tags = Tag::all();

        return view('home.blog.index', compact('posts', 'categories', 'tags', 'search'));
    }

    public function blog_detail($slug)
    {
        $post = Post::where('slug', $slug)->first();
        return view('home.blog.detail', compact('post'));
    }

    public function blog_category($slug)
    {
        $categories = Category::where('slug', $slug)->first();
        $posts = $categories->posts()->where('status', 'publish')->latest()->paginate(4);

        $found = Category::find($categories->id);
        $categories = Category::all();
        $tags = Tag::all();
        return view('home.blog.category', compact('posts', 'categories', 'tags', 'found'));
    }

    public function blog_tag($slug)
    {
        $tags = Tag::where('slug', $slug)->first();
        $posts = $tags->posts()->where('status', 'publish')->latest()->paginate(4);

        $found = Tag::find($tags->id);
        $categories = Category::all();
        $tags = Tag::all();
        return view('home.blog.tag', compact('posts', 'categories', 'tags', 'found'));
    }
    // End Section Blog

    // About
    public function about_index()
    {
        $page = Page::find(1);
        return view('home.about', compact('page'));
    }
    // End About

    // Contact
    public function contact_index()
    {
        return view('home.contact');
    }

    public function message_create(Request $request)
    {
        Message::create([
            'name' => $request->name,
            'email' => $request->email,
            'message' => $request->message,
            'status' => 'warning',
        ]);
        return redirect('/contact#message')->with('success', $request->name);
    }
    // /Contact
}
