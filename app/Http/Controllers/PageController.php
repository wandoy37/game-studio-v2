<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PageController extends Controller
{
    public function edit($slug)
    {
        $page = Page::query()->where('slug', $slug)->first();
        return view('admin.pages.edit', compact('page'));
    }

    public function update(Request $request, $id)
    {
        $page = Page::find($id);

        $data = [
            'title' => $request->title,
            'slug' => Str::slug($request->title, '-'),
            'content' => $request->content
        ];

        $page->update($data);
        $alert = 'Pages ' . $request->title . ' has been updated!';
        return redirect('/admin/pages')->with('success', $alert);
    }
}
