<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;


class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Post::class;

    public function definition()
    {

        return [
            'title' => $this->faker->sentence(3),
            'slug' => Str::slug($this->faker->sentence(3)),
            'thumbnail' => 'https://i.pinimg.com/564x/b7/04/5a/b7045ac42fadd338b3a707b07640f774.jpg',
            'description' => $this->faker->paragraph(),
            'content' => $this->faker->text(1000),
            'status' => 'publish',
            'user_id' => '1',
            'category_id' => $this->faker->numberBetween(1, 7),
        ];
    }
}
