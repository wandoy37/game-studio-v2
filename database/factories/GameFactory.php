<?php

namespace Database\Factories;

use App\Models\Game;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class GameFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */

    protected $model = Game::class;

    public function definition()
    {
        return [
            'title' => $this->faker->sentence(3),
            'slug' => Str::slug($this->faker->sentence(3)),
            'thumbnail' => 'https://i.pinimg.com/564x/ad/ee/60/adee604e01e4321e8773c8243cafef73.jpg',
            'description' => $this->faker->paragraph(),
            'content' => $this->faker->text(1000),
            'status' => 'publish',
            'user_id' => '1',
            'genre_id' => $this->faker->numberBetween(1, 6),
        ];
    }
}
