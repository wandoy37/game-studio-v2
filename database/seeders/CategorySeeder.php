<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'title' => 'Epic',
            'slug' => 'epic'
        ]);
        Category::create([
            'title' => 'Esports',
            'slug' => 'esports'
        ]);
        Category::create([
            'title' => 'Coming Soon',
            'slug' => 'coming-soon'
        ]);
        Category::create([
            'title' => 'Legendary',
            'slug' => 'legendary'
        ]);
        Category::create([
            'title' => 'Adventure',
            'slug' => 'Adventure'
        ]);
        Category::create([
            'title' => 'Single Player',
            'slug' => 'single-player'
        ]);
        Category::create([
            'title' => 'Extreme',
            'slug' => 'extreme'
        ]);
    }
}
