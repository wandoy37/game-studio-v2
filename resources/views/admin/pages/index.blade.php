@extends('admin.layouts.app')

@section('content')

<div class="container">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Page</h4>
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url("/admin/dashboard") }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Pages</li>
            </ol>
        </nav>
        <div class="page-category">

        </div>

        {{-- Start --}}
        <div class="row">
            <div class="col-md">
                <form action="/admin/category/">
                    <div class="form-group">
                        <div class="input-icon">
                            <input type="text" name="search" class="form-control" placeholder="Search for...">
                            <span class="input-icon-addon">
                                <i class="fa fa-search"></i>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md">
                <div class="form-group">
                    <a href="{{ url("/admin/page/create") }}" class="btn btn-primary">
                        <i class="fas fa-plus"></i>
                        Page
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    @if (session('success'))
                        <div class="alert alert-primary" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Page</th>
                            <th scope="col" width="20%">Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pages as $page)
                            <tr>
                                <td><a href="/">{{ $page->title }}</a></td>
                                <td>
                                    <form action="{{ url("/pages/$page->slug/edit") }}" method="post">
                                        @csrf @method('DELETE')
                                        <a href="{{ url("/admin/pages/$page->slug/edit") }}" class="btn btn-primary btn-round">
                                            <i class="fas fa-pen"></i>
                                        </a>
                                        @if (!$page->id == 1)
                                            <button type="submit" class="btn btn-danger btn-round" onclick="return confirm('Apakah anda yakin ingin menghapus page ini?')">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        @endif
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md">
                <div class="d-flex justify-content-center">
                    {{-- {{ $pages->links('pagination::bootstrap-4') }} --}}
                </div>
            </div>
        </div>
        {{-- End --}}




    </div>
</div>

@endsection
