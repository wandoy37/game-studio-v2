@extends('home.layouts.app')

@section('content')

<!-- inner-hero start -->
<section class="inner-hero bg_img" data-background="{{ asset('assets/images/bg/inner-hero.jpg') }}">
    <div class="shape position-absolute"><img src="{{ asset('assets/images/elements/inner-hero/shape.png') }}" alt="image" ></div>

    <div class="el-1 position-absolute"><img src="{{ asset('assets/images/elements/inner-hero/el-1.png') }}" alt="image"></div>
    <div class="el-2 position-absolute"><img src="{{ asset('assets/images/elements/inner-hero/el-2.png') }}" alt="image"></div>
    <div class="el-3 position-absolute"><img src="{{ asset('assets/images/elements/inner-hero/el-3.png') }}" alt="image"></div>
    <div class="el-4 position-absolute"><img src="{{ asset('assets/images/elements/inner-hero/el-4.png') }}" alt="image"></div>
    <div class="el-5 position-absolute"><img src="{{ asset('assets/images/elements/inner-hero/el-5.png') }}" alt="image"></div>
    <div class="el-6 position-absolute"><img src="{{ asset('assets/images/elements/inner-hero/el-6.png') }}" alt="image"></div>

    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">BLOG</h2>
          <ul class="page-list">
            <li><a href="index.html">Home</a></li>
            <li>Blog. 1</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <!-- inner-hero end -->

  <!-- blog section start -->
  <section id="blog" class="pt-120 pb-120">
    <div class="container">
      <div class="row mb-none-40">
        <div class="col-lg-8">

        @forelse ($posts as $post)
            <div class="post-card style--two mb-40">
                <div class="post-card__thumb">
                    <img src="{{ url("$post->thumbnail") }}" alt="image">
                </div>
                <div class="post-card__content">
                    <h3 class="post-card__title mb-2"><a href="{{ url("/blog/$post->slug#blog") }}">{{ $post->title }}</a></h3>
                    <p>{{ Str::limit($post->description, 150, '...') }}</p>
                    <span class="time">{{ $post->created_at->format('d, M Y') }}</span>
                </div>
            </div>
        @empty
            <div class="col-md">
                <h4 class="text-center">Konten "{{ $search }}" tidak ditemukan</h4>
            </div>
        @endforelse

          <div class="row mt-4">
            <div class="col-lg-12">
                <div class="d-flex justify-content-center">
                    {{ $posts->links('pagination::bootstrap-4') }}
                </div>
            </div>
          </div>
        </div>

        <div class="col-lg-4 mt-lg-0 mt-5">
          <div class="sidebar">
            <div class="widget">
              <h3 class="widget__title">Search</h3>
              <form class="widget__search" action="/blog#blog">
                <input type="text" name="search" placeholder="Search.." class="form-control" value="{{ $search }}">
                <button type="submit"><i class="fas fa-search"></i> search</button>
              </form>
            </div><!-- widget end -->
            <div class="widget">
              <h3 class="widget__title">Follow Us</h3>
              <ul class="social-links">
                <li><a href="#0"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="#0"><i class="fab fa-twitter"></i></a></li>
                <li><a href="#0"><i class="fab fa-instagram"></i></a></li>
                <li><a href="#0"><i class="fab fa-pinterest-p"></i></a></li>
                <li><a href="#0"><i class="fab fa-google-plus-g"></i></a></li>
              </ul>
            </div><!-- widget end -->
            <div class="widget">
              <h3 class="widget__title">Categories</h3>
              <ul class="category-list">
                @foreach ($categories as $category)
                    <li>
                        <a href="/blog/category/{{ $category->slug }}#blog">
                        <span class="caption">{{ $category->title }}</span>
                        {{-- <span class="value">50</span> --}}
                        </a>
                    </li>
                @endforeach
              </ul>
            </div><!-- widget end -->
            <div class="widget">
              <h3 class="widget__title">Tags</h3>
              <div class="tags-list">
                @foreach ($tags as $tag)
                    <a href="/blog/tag/{{ $tag->slug }}#blog">{{ $tag->title }}</a>
                @endforeach
              </div>
            </div><!-- widget end -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- blog section end -->

@endsection
