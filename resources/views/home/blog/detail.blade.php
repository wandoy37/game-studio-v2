@extends('home.layouts.app')

@section('content')

    <!-- inner-hero start -->
    <div class="inner-hero bg_img style--two" data-background="{{ asset('assets/images/bg/inner-hero-2.jpg') }}">
        <div class="shape position-absolute"><img src="{{ asset('assets/images/elements/inner-hero/shape.png') }}" alt="image" ></div>
      </div>
      <!-- inner-hero end -->

      <!-- blog details section start -->
      <section class="blog-details pb-120">
        <div class="container">
          <div class="row justify-content-center border-bottom pb-5">
            <div class="col-12">
              <div class="blog-details__header">
                {{-- <h2 class="blog-details__title">{{ $post->title }}</h2> --}}
                <div class="row">
                  <div class="col-lg-6">
                    <ul class="meta-post">
                      <li><span>{{ $post->created_at->format('d, M Y') }} By</span></li>
                      <li>
                        <div class="post-author">
                          <div class="post-author__thumb">
                            @if ($post->user->avatar)
                                <img src="{{ asset('storage/' . $post->user->avatar) }}" alt="preview" class="avatar-img rounded-circle">
                            @else
                                <img src="{{ asset('/user/anonim.jpg') }}" alt="preview" class="avatar-img rounded-circle">
                            @endif
                          </div>
                          <span class="post-author__name text-capitalize">{{ $post->user->name }}</span>
                        </div>
                      </li>
                    </ul>
                    {{-- Category and Tags --}}
                    <ul class="meta-post">
                        <li>Category <a href="#0" class="badge badge-primary pt-2">{{ $post->category->title }}</a></li>
                        <br>
                        <li>
                            <i class="fas fa-tags"></i>
                            @foreach ($post->tags as $tag)
                                <a href="http://">{{ $tag->title }},</a>
                            @endforeach
                        </li>
                      </ul>
                  </div>
                  <div class="col-lg-6">
                    <ul class="social-links">
                      <li><span>Share :</span></li>
                      <li><a href="#0"><i class="fab fa-facebook-f"></i></a></li>
                      <li><a href="#0"><i class="fab fa-twitter"></i></a></li>
                      <li><a href="#0"><i class="fab fa-instagram"></i></a></li>
                      <li><a href="#0"><i class="fab fa-pinterest-p"></i></a></li>
                      <li><a href="#0"><i class="fab fa-google-plus-g"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div id="blog" class="col-lg-10 mt-100">
              <div class="blog-details__content">
                <h2 class="">{{ $post->title }}</h2>
                <p>
                    {!! $post->content !!}
                </p>
                <div class="img-holder text-center mt-5 mb-5">
                  <img src="{{ url("$post->thumbnail") }}" alt="image">
                    <blockquote>
                        <p>“{{ $post->description }}”</p>
                    </blockquote>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- blog details section end -->

@endsection
