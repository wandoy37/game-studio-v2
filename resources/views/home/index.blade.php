@extends('home.layouts.app')

@section('content')

  <!-- hero section start -->
  <section class="hero bg_img" data-background="{{ asset('assets/images/bg/hero.jpg') }}">
    <div class="hero__shape">
      <img src="{{ asset('assets/images/elements/hero/shape.png') }}" alt="image">
    </div>
    <div class="el-1"><img src="{{ asset('assets/images/elements/hero/el-1.png') }}" alt="image"></div>
    <div class="el-2"><img src="{{ asset('assets/images/elements/hero/el-2.png') }}" alt="image"></div>
    <div class="el-3"><img src="{{ asset('assets/images/elements/hero/el-3.png') }}" alt="image"></div>
    <div class="el-4"><img src="{{ asset('assets/images/elements/hero/el-4.png') }}" alt="image"></div>
    <div class="el-5"><img src="{{ asset('assets/images/elements/hero/el-5.png') }}" alt="image"></div>
    <div class="el-6"><img src="{{ asset('assets/images/elements/hero/el-6.png') }}" alt="image"></div>
    <div class="el-7"><img src="{{ asset('assets/images/elements/hero/el-7.png') }}" alt="image"></div>
    <div class="el-8"><img src="{{ asset('assets/images/elements/hero/el-8.png') }}" alt="image"></div>
    <div class="el-9"><img src="{{ asset('assets/images/elements/hero/el-9.png') }}" alt="image"></div>
    <div class="el-10"><img src="{{ asset('assets/images/elements/hero/el-10.png') }}" alt="image"></div>
    <div class="el-11"><img src="{{ asset('assets/images/elements/hero/el-11.png') }}" alt="image"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div class="hero__content">
            <span class="hero__sub-title wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">GAME STUDIOS</span>
            <h2 class="hero__title wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">IMAJORA
            </h2>
            <p class="wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.7s">Jelajahi game terbaikmu, dapatkan sumber informasi seputar game dan pertandaingan</p>
            <a href="{{ route('blog.index') }}" class="cmn-btn wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">View Blog</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- hero section end -->

  <!-- game section start -->
  <section class="pt-120 pb-120 position-relative overflow-hidden">
    <div class="game-el-1" data-paroller-factor="-0.1" data-paroller-type="foreground"
      data-paroller-direction="vertical"><img src="{{ asset('assets/images/elements/game-el-1.png') }}" alt="image"></div>
    <div class="game-el-2" data-paroller-factor="-0.1" data-paroller-type="foreground"
      data-paroller-direction="horizontal"><img src="{{ asset('assets/images/elements/game-el-2.png') }}" alt="image"></div>
    <div class="game-el-3" data-paroller-factor="0.1" data-paroller-type="foreground"
      data-paroller-direction="horizontal"><img src="{{ asset('assets/images/elements/game-el-3.png') }}" alt="image"></div>
    <div class="game-el-4" data-paroller-factor="0.25" data-paroller-type="foreground"
      data-paroller-direction="vertical"><img src="{{ asset('assets/images/elements/game-el-4.png') }}" alt="image"></div>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-5">
          <div class="section-header text-center has--bg">
            <div class="header-image"><img src="{{ asset('assets/images/elements/header-el.png') }}" alt="image"></div>
            <h2 class="section-title">Games</h2>
          </div>
        </div>
      </div><!-- row end -->

      <div class="row mb-none-40">
        @foreach ($games as $game)
            <div class="col-lg-6 mb-40  wow fadeInUp" data-wow-duration="0.3s" data-wow-delay="0.3s">
            <div class="game-card">
                <div class="game-card__thumb">
                <img src="{{ url("$game->thumbnail") }}" alt="image">
                </div>
                <div class="game-card__content">
                <div class="game-card__details">
                    <div class="game-card__info">
                    <div class="thumb"><img src="{{ url("$game->thumbnail") }}" alt="image"></div>
                    <div class="content">
                        <h3 class="game-card__title"><a href="{{ url("/game/$game->slug") }}">{{ Str::limit($game->title, 10, '..') }}</a></h3>
                        <p>{{ Str::limit($game->description, 25, '...') }} <a href="{{ url("/game/$game->slug") }}">Read More</a></p>
                    </div>
                    </div>
                </div>
                <div class="game-card__action">
                    <div class="tags-list">
                        <a href="/game/genre/{{ $game->genre->slug }}" class="text-white text-uppercase">{{ $game->genre->title }}</a>
                    </div>
                    {{-- <a href="#0"><img src="{{ asset('assets/images/elements/desktop-btn.png') }}" alt="image"></a>
                    <a href="#0"><img src="{{ asset('assets/images/elements/google-btn.png') }}" alt="image"></a> --}}
                </div>
                </div><!-- game-card__content end -->
            </div><!-- game-card end -->
            </div>
        @endforeach
      </div>

    </div>
  </section>
  <!-- game section end -->

  <!-- about section start -->
  <section class=" pt-120 pb-120 position-relative overflow-hidden">
    <div class="about-obj-1" data-paroller-factor="-0.08" data-paroller-type="foreground"
      data-paroller-direction="horizontal"><img src="{{ asset('assets/images/elements/about-obj-1.png') }}" alt="image"></div>
    <div class="about-obj-2" data-paroller-factor="0.08" data-paroller-type="foreground"
      data-paroller-direction="horizontal"><img src="{{ asset('assets/images/elements/about-obj-2.png') }}" alt="image"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div class="about-content">
            <div class="section-header has--bg">
              <div class="header-image style--two"><img src="{{ asset('assets/images/elements/header-el-2.png') }}" alt="image">
              </div>
              <h2 class="section-title">We Focus on Quality Entertainment</h2>
            </div>
            <p>Our main focus is to provide the best entertainment possible for our players. We place key emphasis on
              creating games that are of a high quality in every way possible. This means that we ensure that all our
              games themes are engaging, our features unique and captivating and our art, stunning and precise! We
              don’t cut corners and do whatever is necessary to offer a memorable gameplay experience.</p>
            <a href="about.html" class="cmn-btn mt-5">learn more</a>
          </div>
        </div>
        <div class="col-lg-4 mt-lg-0 mt-4">
          <div class="about-thumb">
            <img src="{{ asset('assets/images/elements/about-player.png') }}" alt="image" class="image-1">
            <img src="{{ asset('assets/images/elements/about-phone.png') }}" alt="image" class="image-2">
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- about section end -->

  <!-- blog section start -->
  <section class="pt-120 pb-90">
    <div class="container">
      <div class="row">
        <div class="col-lg-4">
          <div class="section-header has--bg style--two">
            <div class="header-image style--two"><img src="{{ asset('assets/images/elements/header-el-4.png') }}" alt="image"></div>
            <span class="section-sub-title">RECENT POSTS</span>
            <h2 class="section-title">Game News</h2>
          </div>
          <a href="{{ route('blog.index') }}" class="cmn-btn">View All posts</a>
        </div>
        <div class="col-lg-8 mt-lg-0 mt-5">
          <div class="blog-slider">
            @foreach ($posts as $post)
            <div class="post-card">
              <div class="post-card__thumb">
                <img src="{{ url("$post->thumbnail") }}" alt="image">
              </div>
                <div class="post-card__content">
                    <h3 class="post-card__title mb-3"><a href="{{ url("/blog/$post->slug") }}">{{ Str::limit($post->title, 30, '...') }}</a></h3>
                    <p>{{ Str::limit($post->description, 50, '...') }}</p>
                    <span class="time">3 min read</span>
                </div>
            </div>
            @endforeach

          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- blog section end -->

@endsection
