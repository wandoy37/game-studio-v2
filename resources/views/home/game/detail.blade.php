@extends('home.layouts.app')

@section('content')

    <!-- inner-hero start -->
    {{-- <div class="inner-hero bg_img style--two" data-background="{{ asset('assets/images/bg/featured.png') }}">
        <div class="shape position-absolute"><img src="{{ asset('assets/images/elements/inner-hero/shape.png') }}" alt="image" ></div>
    </div> --}}
    <!-- inner-hero end -->

        <section class="pt-120 position-relative">
            <div class="container pt-120">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="section-header text-center has--bg">
                            <h2 class="section-title">{{ $game->title }}</h2>
                            <img src="{{ url("$game->thumbnail") }}" alt="image">
                        </div>
                        <div class="blog-details__content">
                            <p>{!! $game->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- single game end -->


        <!-- game info start -->
        <section class="pt-120 pb-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-header has--bg mb-5">
                        <div class="header-image"><img src="{{ asset('assets/images/elements/header-el-7.png') }}" alt="image"></div>
                        <h2 class="section-title text-center">Game Info</h2>
                        <div class="text-center">
                            <p>Ganre</p>
                            <h3 class="text-uppercase mt-3">{{ $game->genre->title }}</h3>
                            <p class="mt-4">Compatible with</p>
                            <div class="tags-list">
                                @foreach ($game->devices as $device)
                                    @if ($device->title == 'desktop')
                                    <a class="text-white text-uppercase">
                                        <i class="fab fa-windows"></i>
                                        {{ $device->title }}
                                    </a>
                                    @else
                                        <a class="text-white text-uppercase">
                                            <i class="fab fa-android"></i>
                                            {{ $device->title }}
                                        </a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="section-header has--bg mb-5">
                        <div class="header-image"><img src="{{ asset('assets/images/elements/header-el-7.png') }}" alt="image"></div>
                        <h2 class="section-title text-center">Description</h2>
                        <p class="text-center">{{ $game->description }}</p>
                    </div>
                </div>
            </div>
        </section>
        <!-- game info end -->
        <!--=====================Game Trailer Section Start================-->
        @isset($game->trailer)
            <section class="container trailer_container pt-4 mt-4">
                <div class="section-header has--bg mb-5">
                    <div class="header-image justify-content-center"><img src="{{ asset('assets/images/elements/header-el-7.png') }}" alt="image"></div>
                    <h2 class="section-title text-center">Game Trailer</h2>
                </div>
                <div class="trailer_items">
                    <div class="trailer_thumbnail" id="thumbnail" onclick="closeThumbnail(this)">
                    <img src="{{ url("$game->thumbnail") }}" alt="First Game Trailer">
                    <a href="javascript:void(0)" class="play_button">
                        <i class="fas fa-play"></i>
                    </a>
                    </div>
                    <div class="trailer_video">
                    <iframe src="{{ $game->trailer }}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </section>
        @endisset


@endsection
