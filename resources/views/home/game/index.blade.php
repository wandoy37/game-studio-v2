@extends('home.layouts.app')

@section('content')

<!-- inner-hero start -->
<section class="inner-hero bg_img" data-background="assets/images/bg/inner-hero.jpg">
    <div class="shape position-absolute"><img src="assets/images/elements/inner-hero/shape.png" alt="image"></div>

    <div class="el-1 position-absolute"><img src="assets/images/elements/inner-hero/el-1.png" alt="image"></div>
    <div class="el-2 position-absolute"><img src="assets/images/elements/inner-hero/el-2.png" alt="image"></div>
    <div class="el-3 position-absolute"><img src="assets/images/elements/inner-hero/el-3.png" alt="image"></div>
    <div class="el-4 position-absolute"><img src="assets/images/elements/inner-hero/el-4.png" alt="image"></div>
    <div class="el-5 position-absolute"><img src="assets/images/elements/inner-hero/el-5.png" alt="image"></div>
    <div class="el-6 position-absolute"><img src="assets/images/elements/inner-hero/el-6.png" alt="image"></div>

    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Games</h2>
          <ul class="page-list">
            <li><a href="index.html">Home</a></li>
            <li>Games</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <!-- inner-hero end -->

  <!-- game section start -->
  <section id="game" class="pt-120 pb-120">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-12">
          <div class="section-header text-center has--bg">
            <div class="header-image"><img src="assets/images/elements/header-el-5.png" alt="image"></div>
                <h2 class="section-title">Genre</h2>
                <div class="tags-list">
                    @foreach ($genres as $genre)
                        <a href="{{ url("/game/genre/$genre->slug#game") }}" class="text-white text-uppercase">{{ $genre->title }}</a>
                    @endforeach
                </div>
          </div>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-lg-8">
            <form class="games-search-from" action="/game#game">
                <input type="text" name="search" placeholder="Search" class="form-control style--two" value="{{ $search }}">
                <button type="submit"><i class="las la-search"></i></button>
            </form>
        </div>
        </div>
            <div class="row mt-50 mb-none-40">
                @forelse ($games as $game)
                    <div class="col-md-6">
                        <div class="post-card style--two mb-40">
                            <div class="post-card__thumb">
                                <img src="{{ url("$game->thumbnail") }}" alt="image">
                                </div>
                                <div class="post-card__content text-center">
                                <h3 class="post-card__title"><a href="{{ url("/game/$game->slug") }}">{{ Str::limit($game->title, 15, '..') }}</a></h3>
                                <p>{{ Str::limit($game->description, 60, '..') }}</p>
                            </div>
                        </div>
                    </div>
                @empty
                <div class="col-md">
                    <h4 class="text-center">Konten "{{ $search }}" tidak ditemukan</h4>
                </div>
                @endforelse
            </div>
        </div>
        <div class="row justify-content-center mt-4 pt-4">
            {{ $games->links('pagination::bootstrap-4') }}
        </div>
  </section>
  <!-- game section end -->

@endsection
