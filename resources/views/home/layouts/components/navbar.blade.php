<!-- login modal -->
{{-- <div class="modal fade" id="loginModal" tabindex="1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content bg-transparent">
      <div class="modal-body">
        <div class="account-area">
          <h3 class="title mb-4">Welcome to Ophela</h3>
            <form class="account-form">
                <div class="form-group">
                    <label>Your Email </label>
                    <input type="email" name="login_email" placeholder="Enter Your Email" class="form-control">
                </div>
                <div class="form-group">
                    <label>Password </label>
                    <input type="password" name="login_email" placeholder="Enter Your Password" class="form-control">
                </div>
                <div class="form-group">
                    <p>Forgot your password? <a href="#0">recover password</a></p>
                    </div>
                <div class="text-center">
                    <button type="submit" class="cmn-btn">Sign IN</button>
                </div>
            </form>
          <div class="or-devider mt-5"><span>OR</span></div>
          <div class="text-center mt-4">
            <p>Sign up with your work email</p>
            <a href="#0" class="google-btn mt-4 mb-3"><img src="{{ asset('assets/images/icon/google.png') }}" alt="image">Sign Up
              with Google</a>
            <p>Don't have an account? <a href="#0" data-dismiss="modal" data-toggle="modal"
                data-target="#signupModal">Sign up here</a></p>
          </div>
        </div><!-- account-area end -->
      </div>
    </div>
  </div>
</div> --}}

<!-- Sign Up modal -->
{{-- <div class="modal fade" id="signupModal" tabindex="1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content bg-transparent">
      <div class="modal-body">
        <div class="account-area">
          <h3 class="title mb-4">Let's get started</h3>
          <div class="text-center mt-4">
            <a href="#0" class="google-btn mt-4 mb-3"><img src="{{ asset('assets/images/icon/google.png') }}" alt="image">Sign Up
              with Google</a>
          </div>
          <div class="or-devider mt-4"><span>OR</span></div>
          <p class="text-center mt-2 mb-4">Sign up with your work email</p>
          <form class="account-form">
            <div class="form-group">
              <label>Your Email </label>
              <input type="email" name="login_email" placeholder="Enter Your Email" class="form-control">
            </div>
            <div class="text-center">
              <button type="submit" class="cmn-btn">Try It Now</button>
            </div>
          </form>
          <p class="text-center mt-3">Already have an account?<a href="#0" data-dismiss="modal" data-toggle="modal"
              data-target="#loginModal">Sign In</a></p>
        </div><!-- account-area end -->
      </div>
    </div>
  </div>
</div> --}}

<header class="header">
    <div class="header__bottom">
      <div class="container">
        <nav class="navbar navbar-expand-xl align-items-center">
          <a class="site-logo site-title" href="{{ url('/') }}"><img src="{{ asset('assets/images/logo.png') }}" alt="site-logo"><span
              class="logo-icon"><i class="flaticon-fire"></i></span></a>
          <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="menu-toggle"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav main-menu ml-auto">
                <li><a href="{{ route('home.index') }}">Home</a></li>
                <li><a href="{{ route('game.index') }}">Game</a></li>
                <li><a href="{{ route('blog.index') }}">Blog</a></li>
                <li><a href="/about#about">About Us</a></li>
                <li><a href="{{ url('/contact') }}">contact</a></li>
            </ul>
            <div class="nav-right">
                @guest
                    <a href="{{ route('dashboard') }}">join now</a>
                    @else
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                @endguest
            </div>
          </div>
        </nav>
      </div>
    </div><!-- header__bottom end -->
  </header>
