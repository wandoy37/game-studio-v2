
<!-- meta tags and other links -->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Imajor - Gaming Studio</title>
  <!-- site favicon -->
  <link rel="icon" type="image/png" href="{{ asset('assets/images/favicon.png') }}" sizes="16x16">
  <!-- bootstrap 4  -->
  <link rel="stylesheet" href="{{ asset('assets/css/vendor/bootstrap.min.css') }}">
  <!-- fontawesome 5  -->
  <link rel="stylesheet" href="{{ asset('assets/css/all.min.css') }}">
  <!-- line-awesome webfont -->
  <link rel="stylesheet" href="{{ asset('assets/css/line-awesome.min.css') }}">
  <!-- custom select css -->
  <link rel="stylesheet" href="{{ asset('assets/css/vendor/nice-select.css') }}">
  <!-- animate css  -->
  <link rel="stylesheet" href="{{ asset('assets/css/vendor/animate.min.css') }}">
  <!-- lightcase css -->
  <link rel="stylesheet" href="{{ asset('assets/css/vendor/lightcase.css') }}">
  <!-- slick slider css -->
  <link rel="stylesheet" href="{{ asset('assets/css/vendor/slick.css') }}">
  <!-- style main css -->
  <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
</head>

<body>

  <!-- prealoader start  -->
  <div class="preloader">
    <div class="spinner">
      <div class="dot1"></div>
      <div class="dot2"></div>
    </div>
  </div>
  <!-- prealoader end -->

  <!-- scroll-to-top start -->
  <div class="scroll-to-top">
    <span class="scroll-icon">
      <i class="las la-arrow-up"></i>
    </span>
  </div>
  <!-- scroll-to-top end -->
  <!-- page-wrapper start -->
  <div class="page-wrapper">


    <!-- header-section start  -->
    @include('home.layouts.components.navbar')
    <!-- header-section end  -->


    @yield('content')


    <!-- footer section start  -->
    @include('home.layouts.components.footer')
    <!-- footer section end -->
  </div>
  <!-- page-wrapper end -->
  <!-- jQuery library -->
  <script src="{{ asset('assets/js/vendor/jquery-3.5.1.min.js') }}"></script>
  <!-- bootstrap js -->
  <script src="{{ asset('assets/js/vendor/bootstrap.bundle.min.js') }}"></script>
  <!-- custom select js -->
  <script src="{{ asset('assets/js/vendor/jquery.nice-select.min.js') }}"></script>
  <!-- lightcase js -->
  <script src="{{ asset('assets/js/vendor/lightcase.js') }}"></script>
  <!-- wow js -->
  <script src="{{ asset('assets/js/vendor/wow.min.js') }}"></script>
  <!-- slick slider js -->
  <script src="{{ asset('assets/js/vendor/slick.min.js') }}"></script>
  <script src="{{ asset('assets/js/vendor/TweenMax.min.js') }}"></script>
  <!-- contact js -->
  <script src="{{ asset('assets/js/contact.js') }}"></script>

  <script src="{{ asset('assets/js/vendor/jquery.paroller.min.js') }}"></script>

  <!-- custom js -->
  <script src="{{ asset('assets/js/app.js') }}"></script>
</body>

</html>
