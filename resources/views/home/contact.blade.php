@extends('home.layouts.app')

@section('content')

    <!-- inner-hero start -->
    <section class="inner-hero bg_img" data-background="assets/images/bg/inner-hero.jpg">
        <div class="shape position-absolute"><img src="assets/images/elements/inner-hero/shape.png" alt="image" ></div>

        <div class="el-1 position-absolute"><img src="assets/images/elements/inner-hero/el-1.png" alt="image"></div>
        <div class="el-2 position-absolute"><img src="assets/images/elements/inner-hero/el-2.png" alt="image"></div>
        <div class="el-3 position-absolute"><img src="assets/images/elements/inner-hero/el-3.png" alt="image"></div>
        <div class="el-4 position-absolute"><img src="assets/images/elements/inner-hero/el-4.png" alt="image"></div>
        <div class="el-5 position-absolute"><img src="assets/images/elements/inner-hero/el-5.png" alt="image"></div>
        <div class="el-6 position-absolute"><img src="assets/images/elements/inner-hero/el-6.png" alt="image"></div>

        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="page-titl=-e">Contact</h2>
              <ul class="page-list">
                <li><a href="index.html">Home</a></li>
                <li>Contact</li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <!-- inner-hero end -->

      <!-- contact section start -->
      <section class="pt-120 pb-120 position-relative overflow-hidden">
        <div class="contact-left-el"><img src="assets/images/elements/contact-left-el.png" alt="image"></div>
        <div class="contact-right-el"><img src="assets/images/elements/contact-right-el.png" alt="image"></div>
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-8">
              <div class="section-header text-center has--bg">
                <div class="header-image"><img src="assets/images/elements/header-el.png" alt="image"></div>
                <h2 class="section-title">Get in Touch</h2>
                <p>If you have a question or general enquiry, we’re here to help. Complete the form below and we will get back to you as soon as we can.</p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="contact-form-wrapper">
                <form class="contact-form" action="{{ route('create.message') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Your Full Name</label>
                        <input type="text" name="name" id="name" placeholder="Enter Your Full Name" class="form-control style--two" required>
                    </div>
                    <div class="form-group">
                        <label>Your Email</label>
                        <input type="email" name="email" id="email" placeholder="Enter Your Email" class="form-control style--two" required>
                    </div>
                    <div class="form-group">
                        <label>Mesaage</label>
                        <textarea class="form-control style--two" placeholder="Enter Your Message" id="message" name="message" required></textarea>
                    </div>
                    <div>
                        <button type="submit" class="submit-btn">Send Message</button>
                    </div>
                </form>
              </div>
            </div>
            <div class="col-lg-4 offset-lg-1 mt-lg-0 mt-5">
              <div class="contact-content-wrapper">
                <h2>Have questions?</h2>
                <p>Have questions about payments or price plans? We have answers!</p>
                <div class="row mt-50 mb-none-30">
                  <div class="col-lg-12 mb-30">
                    <div class="contact-item">
                      <div class="contact-item__icon">
                        <i class="las la-envelope"></i>
                      </div>
                      <div class="contact-item__content">
                        <h3 class="title">Email Us</h3>
                        <a href="/cdn-cgi/l/email-protection#adcbccc5ccc9c9c8dbdeedcac0ccc4c183cec2c0"><span class="__cf_email__" data-cfemail="abc2c5cdc4ebe4fbc3cec7ca85c8c4c6">[email&#160;protected]</span></a>
                      </div>
                    </div><!-- contact-item end -->
                  </div>
                  <div class="col-lg-12 mb-30">
                    <div class="contact-item">
                      <div class="contact-item__icon">
                        <i class="las la-phone"></i>
                      </div>
                      <div class="contact-item__content">
                        <h3 class="title">Call Us</h3>
                        <p><a href="tel:65455">+1 (987) 664-32-11</a></p>
                        <p><a href="tel:65455">+1 (987) 664-32-11</a></p>
                      </div>
                    </div><!-- contact-item end -->
                  </div>
                  <div class="col-lg-12 mb-30">
                    <div class="contact-item">
                      <div class="contact-item__icon">
                        <i class="las la-map-marker-alt"></i>
                      </div>
                      <div class="contact-item__content">
                        <h3 class="title">Visit Us</h3>
                        <p>4293 Euclid Avenue, Los Angeles,CA 90012</p>
                      </div>
                    </div><!-- contact-item end -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- contact section end -->

      <!-- faq section start -->
      <section class="position-relative">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-8">
              <div class="section-header text-center has--bg">
                    @if (session('success'))
                        <div class="header-image" id="message"><img src="assets/images/elements/header-el.png" alt="image"></div>
                        <h4>Terimakasih {{ session('success') }}, kami akan segera menganggapi pesan mu.</h4>
                    @endif
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- faq section end -->

@endsection
