@extends('home.layouts.app')

@section('content')
    
    <!-- hero section start -->
    <section class="hero bg_img" data-background="assets/images/bg/hero.jpg">
      <div class="hero__shape">
        <img src="assets/images/elements/hero/shape-2.png" alt="image">
      </div>
      <div class="el-1"><img src="assets/images/elements/hero/el-1.png" alt="image"></div>
      <div class="el-2"><img src="assets/images/elements/hero/el-2.png" alt="image"></div>
      <div class="el-3"><img src="assets/images/elements/hero/el-3.png" alt="image"></div>
      <div class="el-4"><img src="assets/images/elements/hero/el-4.png" alt="image"></div>
      <div class="el-5"><img src="assets/images/elements/hero/el-5.png" alt="image"></div>
      <div class="el-6"><img src="assets/images/elements/hero/el-6.png" alt="image"></div>
      <div class="el-7"><img src="assets/images/elements/hero/el-7.png" alt="image"></div>
      <div class="el-8"><img src="assets/images/elements/hero/el-8.png" alt="image"></div>
      <div class="el-9"><img src="assets/images/elements/hero/el-9.png" alt="image"></div>
      <div class="el-10"><img src="assets/images/elements/hero/el-10.png" alt="image"></div>
      <div class="el-11"><img src="assets/images/elements/hero/el-11.png" alt="image"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
            <div class="hero__content">
              <span class="hero__sub-title wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">GAME STUDIOS</span>
              <h2 class="hero__title wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">The Future Of Gaming</h2>
              <p class=" wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.7s">Our games entertain millions of people every day, everywhere</p>
              <a href="games.html" class="cmn-btn wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">play now</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- hero section end -->

    <!-- game section start -->
    <section class="pt-120 pb-120">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-5">
            <div class="section-header text-center has--bg">
              <div class="header-image"><img src="assets/images/elements/header-el.png" alt="image"></div>
              <h2 class="section-title">Top Games</h2>
            </div>
          </div>
        </div><!-- row end -->
        <div class="row mb-none-40">
          <div class="col-lg-12 mb-40 single-game">
            <div class="game-card style--two">
              <div class="game-card__content wow fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.7s">
                <div class="game-card__details">
                  <div class="game-card__info">
                    <div class="thumb-wrapper">
                      <div class="thumb"><img src="assets/images/games/s1.jpg" alt="image"></div>
                      <div class="game-card__ratings">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half-alt"></i>
                        <span>(4.5)</span>
                      </div>
                    </div>
                    <div class="content">
                      <h3 class="game-card__title"><a href="game-details.html">Tomb of Treasure</a></h3>
                      <p>Fusce sit amet erat sagittis, finibus libero sit amet, aliquet erat. dolor.Duis luctus felis sit amet libero.</p>
                      <a href="game-details.html" class="mt-3">Read More</a>
                    </div>
                  </div>
                </div>
              </div><!-- game-card__content end -->
              <div class="game-card__thumb wow zoomIn" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <img src="assets/images/games/b1.jpg" alt="image">
              </div>
              <div class="game-card__footer wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">
                <div class="left">
                  <div class="thumb"><img src="assets/images/games/m1.jpg" alt="image" class="w-100"></div>
                  <div class="thumb"><img src="assets/images/games/m2.jpg" alt="image" class="w-100"></div>
                </div>
                <div class="right">
                  <div class="caption">DOWNLOAD NOW AT</div>
                  <div class="apps-btn">
                    <a href="#0"><img src="assets/images/elements/apple-btn.png" alt="image"></a>
                    <a href="#0"><img src="assets/images/elements/google-btn.png" alt="image"></a>
                  </div>
                </div>
              </div>
            </div><!-- game-card end -->
          </div><!-- single-game end -->
          <div class="col-lg-12 mb-40 single-game">
            <div class="game-card style--two">
              <div class="game-card__content wow fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.7s">
                <div class="game-card__details">
                  <div class="game-card__info">
                    <div class="thumb-wrapper">
                      <div class="thumb"><img src="assets/images/games/s2.jpg" alt="image"></div>
                      <div class="game-card__ratings">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half-alt"></i>
                        <span>(4.5)</span>
                      </div>
                    </div>
                    <div class="content">
                      <h3 class="game-card__title"><a href="game-details.html">Gem Saviour Sword</a></h3>
                      <p>Fusce sit amet erat sagittis, finibus libero sit amet, aliquet erat. dolor.Duis luctus felis sit amet libero.</p>
                      <a href="game-details.html" class="mt-3">Read More</a>
                    </div>
                  </div>
                </div>
              </div><!-- game-card__content end -->
              <div class="game-card__thumb wow zoomIn" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <img src="assets/images/games/b2.jpg" alt="image">
              </div>
              <div class="game-card__footer wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">
                <div class="left">
                  <div class="thumb"><img src="assets/images/games/m3.jpg" alt="image" class="w-100"></div>
                  <div class="thumb"><img src="assets/images/games/m4.jpg" alt="image" class="w-100"></div>
                </div>
                <div class="right">
                  <div class="caption">DOWNLOAD NOW AT</div>
                  <div class="apps-btn">
                    <a href="#0"><img src="assets/images/elements/apple-btn.png" alt="image"></a>
                    <a href="#0"><img src="assets/images/elements/google-btn.png" alt="image"></a>
                  </div>
                </div>
              </div>
            </div><!-- game-card end -->
          </div><!-- single-game end -->
          <div class="col-lg-12 mb-40 single-game">
            <div class="game-card style--two">
              <div class="game-card__content wow fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.7s">
                <div class="game-card__details">
                  <div class="game-card__info">
                    <div class="thumb-wrapper">
                      <div class="thumb"><img src="assets/images/games/s3.jpg" alt="image"></div>
                      <div class="game-card__ratings">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half-alt"></i>
                        <span>(4.5)</span>
                      </div>
                    </div>
                    <div class="content">
                      <h3 class="game-card__title"><a href="game-details.html">Journey To The Wealth</a></h3>
                      <p>Fusce sit amet erat sagittis, finibus libero sit amet, aliquet erat. dolor.Duis luctus felis sit amet libero.</p>
                      <a href="game-details.html" class="mt-3">Read More</a>
                    </div>
                  </div>
                </div>
              </div><!-- game-card__content end -->
              <div class="game-card__thumb wow zoomIn" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <img src="assets/images/games/b3.jpg" alt="image">
              </div>
              <div class="game-card__footer wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">
                <div class="left">
                  <div class="thumb"><img src="assets/images/games/m5.jpg" alt="image" class="w-100"></div>
                  <div class="thumb"><img src="assets/images/games/m6.jpg" alt="image" class="w-100"></div>
                </div>
                <div class="right">
                  <div class="caption">DOWNLOAD NOW AT</div>
                  <div class="apps-btn">
                    <a href="#0"><img src="assets/images/elements/apple-btn.png" alt="image"></a>
                    <a href="#0"><img src="assets/images/elements/google-btn.png" alt="image"></a>
                  </div>
                </div>
              </div>
            </div><!-- game-card end -->
          </div><!-- single-game end -->
          <div class="col-lg-12 mb-40 single-game">
            <div class="game-card style--two">
              <div class="game-card__content wow fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.7s">
                <div class="game-card__details">
                  <div class="game-card__info">
                    <div class="thumb-wrapper">
                      <div class="thumb"><img src="assets/images/games/s4.jpg" alt="image"></div>
                      <div class="game-card__ratings">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half-alt"></i>
                        <span>(4.5)</span>
                      </div>
                    </div>
                    <div class="content">
                      <h3 class="game-card__title"><a href="game-details.html">Tomb of Treasure</a></h3>
                      <p>Fusce sit amet erat sagittis, finibus libero sit amet, aliquet erat. dolor.Duis luctus felis sit amet libero.</p>
                      <a href="game-details.html" class="mt-3">Read More</a>
                    </div>
                  </div>
                </div>
              </div><!-- game-card__content end -->
              <div class="game-card__thumb wow zoomIn" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <img src="assets/images/games/b4.jpg" alt="image">
              </div>
              <div class="game-card__footer wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">
                <div class="left">
                  <div class="thumb"><img src="assets/images/games/m1.jpg" alt="image" class="w-100"></div>
                  <div class="thumb"><img src="assets/images/games/m2.jpg" alt="image" class="w-100"></div>
                </div>
                <div class="right">
                  <div class="caption">DOWNLOAD NOW AT</div>
                  <div class="apps-btn">
                    <a href="#0"><img src="assets/images/elements/apple-btn.png" alt="image"></a>
                    <a href="#0"><img src="assets/images/elements/google-btn.png" alt="image"></a>
                  </div>
                </div>
              </div>
            </div><!-- game-card end -->
          </div><!-- single-game end -->
        </div>
        <div class="row mt-50">
          <div class="col-lg-12 text-center">
            <a href="games.html" class="cmn-btn btn--lg">view all games</a>
          </div>
        </div>
      </div>
    </section>
    <!-- game section end -->

    <!-- trailer section start -->
        <!--=====================Game Trailer Section Start================-->
<section class="trailer_bg">
  <div class="trailer_head">
    <h1>Game Trailer</h1>
  </div>
</section>

<section class="container trailer_container">
  <div class="trailer_items">
    <div class="trailer_thumbnail" id="thumbnail" onclick="closeThumbnail(this)">
      <img src="assets/images/bg/video-thumb.jpg" alt="First Game Trailer">
      <a href="javascript:void(0)" class="play_button">
        <i class="fas fa-play"></i>
      </a>
    </div>
    <div class="trailer_video">
      <iframe src="https://www.youtube.com/embed/Hru2DdJxFZQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>

  <div class="trailer_items">
    <div class="trailer_thumbnail" id="thumbnail" onclick="closeThumbnail(this)">
      <img src="assets/images/bg/trailer_two.jpg" alt="First Game Trailer">
      <a href="javascript:void(0)" class="play_button">
        <i class="fas fa-play"></i>
      </a>
    </div>
    <div class="trailer_video">
      <iframe src="https://www.youtube.com/embed/F4ym-99UNLM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>

  <div class="trailer_items">
    <div class="trailer_thumbnail" id="thumbnail" onclick="closeThumbnail(this)">
      <img src="assets/images/bg/trailer_three.jpg" alt="First Game Trailer">
      <a href="javascript:void(0)" class="play_button">
        <i class="fas fa-play"></i>
      </a>
    </div>
    <div class="trailer_video">
      <iframe src="https://www.youtube.com/embed/ssrNcwxALS4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>

  <div class="trailer_items">
    <div class="trailer_thumbnail" id="thumbnail" onclick="closeThumbnail(this)">
      <img src="assets/images/bg/trailer_four.jpg" alt="First Game Trailer">
      <a href="javascript:void(0)" class="play_button">
        <i class="fas fa-play"></i>
      </a>
    </div>
    <div class="trailer_video">
      <iframe src="https://www.youtube.com/embed/oSR1cdqkMkU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>

  <div class="trailer_items">
    <div class="trailer_thumbnail" id="thumbnail" onclick="closeThumbnail(this)">
      <img src="assets/images/bg/trailer_five.jpg" alt="First Game Trailer">
      <a href="javascript:void(0)" class="play_button">
        <i class="fas fa-play"></i>
      </a>
    </div>
    <div class="trailer_video">
      <iframe src="https://www.youtube.com/embed/7YnopWO4X_Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>

  <div class="trailer_details">
    <a class="prev" onclick="plusSlides(-1)">Prev</a>
    <a class="next" onclick="plusSlides(1)">Next</a>
    <div class="game_content">
      <h4>The Crazy Farm Game</h4>
      <a href="game-details.html" target="_blank">View Details</a>
    </div>
    <div class="game_content">
      <h4>The Second Farm Game</h4>
      <a href="games.html" target="_blank">View Details</a>
    </div>
    <div class="game_content">
      <h4>The Third Farm Game</h4>
      <a href="about.html" target="_blank">View Details</a>
    </div>
    <div class="game_content">
      <h4>The Fourth Farm Game</h4>
      <a href="blog-details.html" target="_blank">View Details</a>
    </div>
    <div class="game_content">
      <h4>The Fifth Farm Game</h4>
      <a href="blog-two.html" target="_blank">View Details</a>
    </div>
  </div>
</section>
<!--=====================Game Trailer Section End================-->
    <!-- trailer section end -->

    <!-- about section start -->
    <section class=" pt-120 pb-120 position-relative overflow-hidden">
      <div class="about-obj-1" data-paroller-factor="-0.08" data-paroller-type="foreground" data-paroller-direction="horizontal"><img src="assets/images/elements/about-obj-1.png" alt="image"></div>
      <div class="about-obj-2" data-paroller-factor="0.08" data-paroller-type="foreground" data-paroller-direction="horizontal"><img src="assets/images/elements/about-obj-2.png" alt="image"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
            <div class="about-content">
              <div class="section-header has--bg">
                <div class="header-image style--two"><img src="assets/images/elements/header-el-2.png" alt="image"></div>
                <h2 class="section-title">We Focus on Quality Entertainment</h2>
              </div>
              <p>Our main focus is to provide the best entertainment possible for our players. We place key emphasis on creating games that are of a high quality in every way possible. This means that we ensure that all our games themes are engaging, our features unique and captivating and our art, stunning and precise! We don’t cut corners and do whatever is necessary to offer a memorable gameplay experience.</p>
              <a href="about.html" class="cmn-btn mt-5">learn more</a>
            </div>
          </div>
          <div class="col-lg-4 mt-lg-0 mt-4">
            <div class="about-thumb">
              <img src="assets/images/elements/about-player.png" alt="image" class="image-1">
              <img src="assets/images/elements/about-phone.png" alt="image" class="image-2">
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- about section end -->

    <!-- overview section start -->
    <section class="pt-120 pb-120 position-relative">
      <div class="bg-el h-75"><img src="assets/images/bg/overview.jpg" alt="image" class="h-100"></div>
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-5">
            <div class="section-header text-center has--bg">
              <div class="header-image"><img src="assets/images/elements/header-el.png" alt="image"></div>
              <h2 class="section-title">Track Record</h2>
            </div>
          </div>
        </div>
        <div class="row justify-content-center mb-none-30">
          <div class="col-lg-4 col-md-6 mb-30 wow zoomIn" data-wow-duration="0.5s" data-wow-delay="0.3s">
            <div class="overview-card">
              <div class="overview-card__icon">
                <img src="assets/images/icon/overview/1.png" alt="image">
              </div>
              <div class="overview-card__content">
                <h4 class="overview-card__number">+12,000</h4>
                <p>Games</p>
              </div>
            </div><!-- overview-card end -->
          </div>
          <div class="col-lg-4 col-md-6 mb-30 wow zoomIn" data-wow-duration="0.5s" data-wow-delay="0.5s">
            <div class="overview-card">
              <div class="overview-card__icon">
                <img src="assets/images/icon/overview/2.png" alt="image">
              </div>
              <div class="overview-card__content">
                <h4 class="overview-card__number">+398943</h4>
                <p>players</p>
              </div>
            </div><!-- overview-card end -->
          </div>
          <div class="col-lg-4 col-md-6 mb-30 wow zoomIn" data-wow-duration="0.5s" data-wow-delay="0.7s">
            <div class="overview-card">
              <div class="overview-card__icon">
                <img src="assets/images/icon/overview/3.png" alt="image">
              </div>
              <div class="overview-card__content">
                <h4 class="overview-card__number">+103</h4>
                <p>champions</p>
              </div>
            </div><!-- overview-card end -->
          </div>
        </div><!-- row end -->
        <div class="row justify-content-center wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
          <div class="col-lg-10">
            <div class="testimonial-slider">
              <div class="testimonial-single">
                <div class="testimonial-single__thumb" data-animation="fadeInUp" data-delay=".3s">
                  <img src="assets/images/testimonial/1.png" alt="image">
                </div>
                <div class="testimonial-single__content" data-animation="fadeInUp" data-delay=".5s">
                  <p>Vestibulum tincidunt tincidunt nisl id congue. Nam feugiat libero eu mattis bibendum. Maecenas luctus ante vel vestibulum ultrices.</p>
                  <span class="designation">Jonathan Davis</span>
                  <div class="ratings">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                    <span class="point">(4.5)</span>
                  </div>
                </div>
              </div><!-- testimonial-single end -->
              <div class="testimonial-single">
                <div class="testimonial-single__thumb" data-animation="fadeInUp" data-delay=".3s">
                  <img src="assets/images/testimonial/1.png" alt="image">
                </div>
                <div class="testimonial-single__content" data-animation="fadeInUp" data-delay=".5s">
                  <p>Vestibulum tincidunt tincidunt nisl id congue. Nam feugiat libero eu mattis bibendum. Maecenas luctus ante vel vestibulum ultrices.</p>
                  <span class="designation">Jonathan Davis</span>
                  <div class="ratings">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                    <span class="point">(4.5)</span>
                  </div>
                </div>
              </div><!-- testimonial-single end -->
              <div class="testimonial-single">
                <div class="testimonial-single__thumb" data-animation="fadeInUp" data-delay=".3s">
                  <img src="assets/images/testimonial/1.png" alt="image">
                </div>
                <div class="testimonial-single__content" data-animation="fadeInUp" data-delay=".5s">
                  <p>Vestibulum tincidunt tincidunt nisl id congue. Nam feugiat libero eu mattis bibendum. Maecenas luctus ante vel vestibulum ultrices.</p>
                  <span class="designation">Jonathan Davis</span>
                  <div class="ratings">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                    <span class="point">(4.5)</span>
                  </div>
                </div>
              </div><!-- testimonial-single end -->
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- overview section end -->

    <!-- blog section start -->
    <section class="pt-120 pb-90">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="section-header has--bg style--two">
              <div class="header-image style--two"><img src="assets/images/elements/header-el-4.png" alt="image"></div>
              <span class="section-sub-title">RECENT POSTS</span>
              <h2 class="section-title">Game News</h2>
            </div>
            <a href="blog-one.html" class="cmn-btn">View All posts</a>
          </div>
          <div class="col-lg-8 mt-lg-0 mt-5">
            <div class="blog-slider">
              <div class="post-card">
                <div class="post-card__thumb">
                  <img src="assets/images/blog/1.jpg" alt="image">
                </div>
                <div class="post-card__content">
                  <span class="date">Sep 15, 2020</span>
                  <h3 class="post-title"><a href="blog-details.html">Benefits of Gaming - PC and Video Games</a></h3>
                  <div class="post-author mt-3">
                    <div class="post-author__thumb">
                      <img src="assets/images/blog/author.png" alt="image">
                    </div>
                    <a href="#0" class="post-author__name">Andres Todd</a>
                  </div>
                </div>
              </div><!-- post-card end -->
              <div class="post-card">
                <div class="post-card__thumb">
                  <img src="assets/images/blog/2.jpg" alt="image">
                </div>
                <div class="post-card__content">
                  <a href="#0" class="post-type"><i class="las la-file-alt"></i></a>
                  <span class="date">Sep 15, 2020</span>
                  <h3 class="post-title"><a href="blog-details.html">Benefits of Gaming - PC and Video Games</a></h3>
                  <div class="post-author mt-3">
                    <div class="post-author__thumb">
                      <img src="assets/images/blog/author.png" alt="image">
                    </div>
                    <a href="#0" class="post-author__name">Andres Todd</a>
                  </div>
                </div>
              </div><!-- post-card end -->
              <div class="post-card">
                <div class="post-card__thumb">
                  <img src="assets/images/blog/1.jpg" alt="image">
                </div>
                <div class="post-card__content">
                  <span class="date">Sep 15, 2020</span>
                  <h3 class="post-title"><a href="blog-details.html">Benefits of Gaming - PC and Video Games</a></h3>
                  <div class="post-author mt-3">
                    <div class="post-author__thumb">
                      <img src="assets/images/blog/author.png" alt="image">
                    </div>
                    <a href="#0" class="post-author__name">Andres Todd</a>
                  </div>
                </div>
              </div><!-- post-card end -->
              <div class="post-card">
                <div class="post-card__thumb">
                  <img src="assets/images/blog/2.jpg" alt="image">
                </div>
                <div class="post-card__content">
                  <a href="#0" class="post-type"><i class="las la-file-alt"></i></a>
                  <span class="date">Sep 15, 2020</span>
                  <h3 class="post-title"><a href="blog-details.html">Benefits of Gaming - PC and Video Games</a></h3>
                  <div class="post-author mt-3">
                    <div class="post-author__thumb">
                      <img src="assets/images/blog/author.png" alt="image">
                    </div>
                    <a href="#0" class="post-author__name">Andres Todd</a>
                  </div>
                </div>
              </div><!-- post-card end -->
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- blog section end -->

@endsection