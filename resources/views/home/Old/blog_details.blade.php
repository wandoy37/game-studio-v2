@extends('home.layouts.app')

@section('content')

    <!-- inner-hero start -->
    <div class="inner-hero bg_img style--two" data-background="assets/images/bg/inner-hero-2.jpg">
        <div class="shape position-absolute"><img src="assets/images/elements/inner-hero/shape.png" alt="image" ></div>
      </div>
      <!-- inner-hero end -->

      <!-- blog details section start -->
      <section class="blog-details pb-120">
        <div class="container">
          <div class="row justify-content-center border-bottom pb-5">
            <div class="col-12">
              <div class="blog-details__header">
                <h2 class="blog-details__title">Video Game Trends: What Four Decades of Gaming Reveal</h2>
                <div class="row mt-4">
                  <div class="col-lg-6">
                    <ul class="meta-post">
                      <li><a href="#0">Dece 15, 2020 BY</a></li>
                      <li>
                        <div class="post-author">
                          <div class="post-author__thumb">
                            <img src="assets/images/blog/author.png" alt="image">
                          </div>
                          <a href="#0" class="post-author__name" tabindex="0">Andres Todd</a>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="col-lg-6">
                    <ul class="social-links">
                      <li><span>Share :</span></li>
                      <li><a href="#0"><i class="fab fa-facebook-f"></i></a></li>
                      <li><a href="#0"><i class="fab fa-twitter"></i></a></li>
                      <li><a href="#0"><i class="fab fa-instagram"></i></a></li>
                      <li><a href="#0"><i class="fab fa-pinterest-p"></i></a></li>
                      <li><a href="#0"><i class="fab fa-google-plus-g"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-10 mt-50">
              <div class="blog-details__content">
                <h3 class="blog-details__sub-title">The Top Best-Selling Games of Each Decade</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vel dignissim tellus convallis quam. Id posuere felis donec at tristique libero. Purus amet morbi vitae lectus. Id vitae, ut <a href="#0">ullamcorper placerat</a> rutrum ipsum lorem. Diam felis et pulvinar ut cursus adipiscing in pulvinar. Suspendisse leo posuere diam, sit bibendum volutpat.</p>
                <blockquote>
                  <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vel dignissim tellusisure Lorem convallis quam.”</p>
                </blockquote>
                <h3 class="blog-details__sub-title mt-5">Best-selling 80s video games</h3>
                <p>Lorem ipsum dolor sit amet, <a href="#0">consectetur adipiscing</a> elit. Vel dignissim tellus convallis quam. Id posuere felis donec at tristique libero. Purus amet morbi vitae lectus. Id vitae, ut ullamcorper placerat rutrum ipsum lorem. Diam felis et pulvinar ut cursus adipiscing in pulvinar. Suspendisse leo posuere diam, sit bibendum volutpat.</p>
                <p>Lorem ipsum <a href="#0">dolor sit amet</a>, consectetur adipiscing elit. Suspendisse nec ipsum mi eget lacus tellus. Est tortor sed fermentum erat interdum ac, nec.</p>
                <div class="img-holder text-center mt-5 mb-5">
                  <img src="assets/images/blog/b5.jpg" alt="image">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
                <p>Lorem ipsum dolor sit amet, <a href="#0">consectetur adipiscing</a> elit. Vel dignissim tellus convallis quam. Id posuere felis donec at tristique libero. Purus amet morbi vitae lectus. Id vitae, ut ullamcorper placerat rutrum ipsum lorem. Diam felis et pulvinar ut cursus adipiscing in pulvinar. Suspendisse leo posuere diam, sit bibendum volutpat.</p>
                <h3 class="blog-details__sub-title mt-5">Best-selling 00s video games</h3>
                <p>Lorem ipsum dolor sit amet, <a href="#0">consectetur adipiscing</a> elit. Vel dignissim tellus convallis quam. Id posuere felis donec at tristique libero. Purus amet morbi vitae lectus. Id vitae, ut ullamcorper placerat rutrum ipsum lorem. Diam felis et pulvinar ut cursus adipiscing in pulvinar. Suspendisse leo posuere diam, sit bibendum volutpat.</p>
                <p>Lorem ipsum <a href="#0">dolor sit amet</a>, consectetur adipiscing elit. Suspendisse nec ipsum mi eget lacus tellus. Est tortor sed fermentum erat interdum ac, nec.</p>
                <div class="video-thumb mt-5 mb-5">
                  <img src="assets/images/blog/b6.jpg" alt="image">
                  <a href="https://www.youtube.com/embed/ssrNcwxALS4" data-rel="lightcase:myCollection" class="video-thumb__icon"><i class="fas fa-play"></i></a>
                </div>
                <h3 class="blog-details__sub-title mt-5">Best-selling video games of the 2010s</h3>
                <p>Lorem ipsum dolor sit amet, <a href="#0">consectetur adipiscing</a> elit. Vel dignissim tellus convallis quam. Id posuere felis donec at tristique libero. Purus amet morbi vitae lectus. Id vitae, ut ullamcorper placerat rutrum ipsum lorem. Diam felis et pulvinar ut cursus adipiscing in pulvinar. Suspendisse leo posuere diam, sit bibendum volutpat.</p>
                <p>Lorem ipsum <a href="#0">dolor sit amet</a>, consectetur adipiscing elit. Suspendisse nec ipsum mi eget lacus tellus. Est tortor sed fermentum erat interdum ac, nec.</p>
              </div>
            </div>
          </div><!-- row end -->
          <div class="row justify-content-center mt-50">
            <div class="col-lg-10">
              <div class="author">
                <div class="author__thumb">
                  <img src="assets/images/blog/author-b.png" alt="image">
                </div>
                <div class="author__content">
                  <span class="top-title">About Author</span>
                  <h2 class="name mb-3">Clark Howell</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi dui phasellus elit euismod lobortis amet, lectus. Lacus nulla nunc sapien quam.</p>
                  <ul class="social-links justify-content-start mt-4">
                    <li><a href="#0"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="#0"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="#0"><i class="fab fa-instagram"></i></a></li>
                    <li><a href="#0"><i class="fab fa-pinterest-p"></i></a></li>
                    <li><a href="#0"><i class="fab fa-google-plus-g"></i></a></li>
                  </ul>
                </div>
              </div><!-- author end -->
            </div>
          </div>
        </div>
      </section>
      <!-- blog details section end -->

@endsection
