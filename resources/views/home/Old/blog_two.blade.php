@extends('home.layouts.app')

@section('content')

    <!-- inner-hero start -->
    <section class="inner-hero bg_img" data-background="assets/images/bg/inner-hero.jpg">
        <div class="shape position-absolute"><img src="assets/images/elements/inner-hero/shape.png" alt="image" ></div>

        <div class="el-1 position-absolute"><img src="assets/images/elements/inner-hero/el-1.png" alt="image"></div>
        <div class="el-2 position-absolute"><img src="assets/images/elements/inner-hero/el-2.png" alt="image"></div>
        <div class="el-3 position-absolute"><img src="assets/images/elements/inner-hero/el-3.png" alt="image"></div>
        <div class="el-4 position-absolute"><img src="assets/images/elements/inner-hero/el-4.png" alt="image"></div>
        <div class="el-5 position-absolute"><img src="assets/images/elements/inner-hero/el-5.png" alt="image"></div>
        <div class="el-6 position-absolute"><img src="assets/images/elements/inner-hero/el-6.png" alt="image"></div>

        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="page-title">Our Blog</h2>
              <ul class="page-list">
                <li><a href="index.html">Home</a></li>
                <li>Blog. 1</li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <!-- inner-hero end -->

      <!-- blog section start -->
      <section class="pt-120 pb-120">
        <div class="container">
          <div class="row mb-none-40">
            <div class="col-lg-12">
              <div class="post-card style--three mb-40">
                <div class="post-card__thumb">
                  <img src="assets/images/blog/bb1.jpg" alt="image">
                </div>
                <div class="post-card__content">
                  <h3 class="post-card__title mb-3"><a href="blog-details.html">The Soothing Psychology of Completing Tasks in Games</a></h3>
                  <p>Unless you're talking about push-ups or bicep curls, repetition isn't a word that has too many admirers. It conjures up thoughts...</p>
                  <span class="time">3 min read</span>
                </div>
              </div><!-- post-card end -->
              <div class="post-card style--three mb-40">
                <div class="post-card__thumb">
                  <img src="assets/images/blog/bb2.jpg" alt="image">
                </div>
                <div class="post-card__content">
                  <h3 class="post-card__title mb-3"><a href="blog-details.html">Video Game Trends: What Four Decades of Gaming Reveal</a></h3>
                  <p>Unless you're talking about push-ups or bicep curls, repetition isn't a word that has too many admirers. It conjures up thoughts...</p>
                  <span class="time">3 min read</span>
                </div>
              </div><!-- post-card end -->
              <div class="post-card style--three mb-40">
                <div class="post-card__thumb">
                  <img src="assets/images/blog/bb3.jpg" alt="image">
                </div>
                <div class="post-card__content">
                  <h3 class="post-card__title mb-3"><a href="blog-details.html">Lords of the Fallen Walkthrough Best Tips & Tricks</a></h3>
                  <p>Unless you're talking about push-ups or bicep curls, repetition isn't a word that has too many admirers. It conjures up thoughts...</p>
                  <span class="time">3 min read</span>
                </div>
              </div><!-- post-card end -->
              <div class="post-card style--three mb-40">
                <div class="post-card__thumb">
                  <img src="assets/images/blog/bb4.jpg" alt="image">
                </div>
                <div class="post-card__content">
                  <h3 class="post-card__title mb-3"><a href="blog-details.html">Game vs Meta-game: What Makes You Keep Playing?</a></h3>
                  <p>Unless you're talking about push-ups or bicep curls, repetition isn't a word that has too many admirers. It conjures up thoughts...</p>
                  <span class="time">3 min read</span>
                </div>
              </div><!-- post-card end -->
              <div class="row mt-4">
                <div class="col-lg-12">
                  <nav>
                    <ul class="pagination justify-content-center align-items-center">
                      <li class="page-item disabled">
                        <a class="page-link prev" href="#" tabindex="-1" aria-disabled="true"><i class="las la-angle-double-left"></i>Prev</a>
                      </li>
                      <li class="page-item"><a class="page-link" href="#">1</a></li>
                      <li class="page-item"><a class="page-link" href="#">2</a></li>
                      <li class="page-item active" aria-current="page">
                        <a class="page-link" href="#">3 <span class="sr-only">(current)</span></a>
                      </li>
                      <li class="page-item"><a class="page-link" href="#">4</a></li>
                      <li class="page-item"><a class="page-link" href="#">5</a></li>
                      <li class="page-item">
                        <a class="page-link next" href="#">Next <i class="las la-angle-double-right"></i></a>
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- blog section end -->

@endsection
