@extends('home.layouts.app')

@section('content')

<!-- inner-hero start -->
<section class="inner-hero bg_img" data-background="assets/images/bg/inner-hero.jpg">
    <div class="shape position-absolute"><img src="assets/images/elements/inner-hero/shape.png" alt="image" ></div>

    <div class="el-1 position-absolute"><img src="assets/images/elements/inner-hero/el-1.png" alt="image"></div>
    <div class="el-2 position-absolute"><img src="assets/images/elements/inner-hero/el-2.png" alt="image"></div>
    <div class="el-3 position-absolute"><img src="assets/images/elements/inner-hero/el-3.png" alt="image"></div>
    <div class="el-4 position-absolute"><img src="assets/images/elements/inner-hero/el-4.png" alt="image"></div>
    <div class="el-5 position-absolute"><img src="assets/images/elements/inner-hero/el-5.png" alt="image"></div>
    <div class="el-6 position-absolute"><img src="assets/images/elements/inner-hero/el-6.png" alt="image"></div>

    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">BLOG</h2>
          <ul class="page-list">
            <li><a href="index.html">Home</a></li>
            <li>Blog. 1</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <!-- inner-hero end -->

  <!-- blog section start -->
  <section class="pt-120 pb-120">
    <div class="container">
      <div class="row mb-none-40">
        <div class="col-lg-8">

          <div class="post-card style--two mb-40">
            <div class="post-card__thumb">
              <img src="assets/images/blog/b1.jpg" alt="image">
            </div>
            <div class="post-card__content">
              <h3 class="post-card__title mb-3"><a href="blog-details.html">The Soothing Psychology of Completing Tasks in Games</a></h3>
              <p>Unless you're talking about push-ups or bicep curls, repetition isn't a word that has too many admirers. It conjures up thoughts...</p>
              <span class="time">3 min read</span>
            </div>
          </div>

          <div class="row mt-4">
            <div class="col-lg-12">
              <nav>
                <ul class="pagination justify-content-center align-items-center">
                  <li class="page-item disabled">
                    <a class="page-link prev" href="#" tabindex="-1" aria-disabled="true"><i class="las la-angle-double-left"></i>Prev</a>
                  </li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item active" aria-current="page">
                    <a class="page-link" href="#">3 <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="page-item"><a class="page-link" href="#">4</a></li>
                  <li class="page-item"><a class="page-link" href="#">5</a></li>
                  <li class="page-item">
                    <a class="page-link next" href="#">Next <i class="las la-angle-double-right"></i></a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>

        <div class="col-lg-4 mt-lg-0 mt-5">
          <div class="sidebar">
            <div class="widget">
              <h3 class="widget__title">Search</h3>
              <form class="widget__search">
                <input type="search" name="widget-search" placeholder="Enter your Search Content" class="form-control">
                <button type="submit"><i class="fas fa-search"></i> search</button>
              </form>
            </div><!-- widget end -->
            <div class="widget">
              <h3 class="widget__title">Latest Posts</h3>
              <div class="small-post-slider">
                <div class="post-item">
                  <div class="post-item__thumb"><img src="assets/images/blog/s1.jpg" alt="image"></div>
                  <div class="post-item__content">
                    <h3><a href="#0">Fighting Climate Change With Games</a></h3>
                    <ul class="post-item__meta mt-2">
                      <li><span><i class="fas fa-comments"></i> 20 Comments</span></li>
                      <li><i class="fas fa-eye"></i> <span>466 View</span></li>
                    </ul>
                  </div>
                </div><!-- post-item end -->
                <div class="post-item">
                  <div class="post-item__thumb"><img src="assets/images/blog/s1.jpg" alt="image"></div>
                  <div class="post-item__content">
                    <h3><a href="#0">Fighting Climate Change With Games</a></h3>
                    <ul class="post-item__meta mt-2">
                      <li><span><i class="fas fa-comments"></i> 20 Comments</span></li>
                      <li><i class="fas fa-eye"></i> <span>466 View</span></li>
                    </ul>
                  </div>
                </div><!-- post-item end -->
                <div class="post-item">
                  <div class="post-item__thumb"><img src="assets/images/blog/s1.jpg" alt="image"></div>
                  <div class="post-item__content">
                    <h3><a href="#0">Fighting Climate Change With Games</a></h3>
                    <ul class="post-item__meta mt-2">
                      <li><span><i class="fas fa-comments"></i> 20 Comments</span></li>
                      <li><i class="fas fa-eye"></i> <span>466 View</span></li>
                    </ul>
                  </div>
                </div><!-- post-item end -->
              </div><!-- small-post-slider end -->
            </div><!-- widget end -->
            <div class="widget">
              <h3 class="widget__title">Follow Us</h3>
              <ul class="social-links">
                <li><a href="#0"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="#0"><i class="fab fa-twitter"></i></a></li>
                <li><a href="#0"><i class="fab fa-instagram"></i></a></li>
                <li><a href="#0"><i class="fab fa-pinterest-p"></i></a></li>
                <li><a href="#0"><i class="fab fa-google-plus-g"></i></a></li>
              </ul>
            </div><!-- widget end -->
            <div class="widget">
              <h3 class="widget__title">Categories</h3>
              <ul class="category-list">
                <li>
                  <a href="#0">
                    <span class="caption">Epic</span>
                    <span class="value">50</span>
                  </a>
                </li>
                <li>
                  <a href="#0">
                    <span class="caption">Esports</span>
                    <span class="value">40</span>
                  </a>
                </li>
                <li>
                  <a href="#0">
                    <span class="caption">Coming Soon</span>
                    <span class="value">55</span>
                  </a>
                </li>
                <li>
                  <a href="#0">
                    <span class="caption">Legendary</span>
                    <span class="value">30</span>
                  </a>
                </li>
                <li>
                  <a href="#0">
                    <span class="caption">Adventure</span>
                    <span class="value">26</span>
                  </a>
                </li>
                <li>
                  <a href="#0">
                    <span class="caption">Single Player</span>
                    <span class="value">55</span>
                  </a>
                </li>
                <li>
                  <a href="#0">
                    <span class="caption">Extreme</span>
                    <span class="value">38</span>
                  </a>
                </li>
              </ul>
            </div><!-- widget end -->
            <div class="widget">
              <h3 class="widget__title">Featured Tags</h3>
              <div class="tags-list">
                <a href="#0">HUNTING</a>
                <a href="#0">SOFTWARE</a>
                <a href="#0">APPS</a>
                <a href="#0">ANDROID</a>
                <a href="#0">UX DESIGN</a>
              </div>
            </div><!-- widget end -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- blog section end -->

@endsection
